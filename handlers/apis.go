package handlers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/budj-budj/budjback/pkg/service"
)

type APIHandlers struct {
	budjService service.BudjService
}

func NewAPIHandler(config Config) *APIHandlers {
	budjConfig := service.BudjConfig{SMSDirectory: config.SMSDirectory}
	budjeService, err := service.NewBudjService(budjConfig)
	if err != nil {
		panic("cannot create BudjService")
	}
	return &APIHandlers{budjService: budjeService}
}

func (h *APIHandlers) AddReceipt(w http.ResponseWriter, r *http.Request) {
	receiptRequest := &service.AddReceiptRequest{}
	req, _ := ioutil.ReadAll(r.Body)
	err := json.Unmarshal(req, receiptRequest)
	if err != nil {
		logrus.WithError(err).Error("cannot unmarshal receipt request")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	resp, err := h.budjService.AddReceipt(receiptRequest)
	if err != nil {
		logrus.WithError(err).Error("cannot add receipt")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	receiptResponse, err := json.Marshal(resp)
	if err != nil {
		logrus.WithError(err).Info("cannot marshal receipt request")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(receiptResponse)
}

func (h *APIHandlers) GetSMSTemplate(w http.ResponseWriter, r *http.Request) {
	smsRequest := &service.GetSMSTemplateRequest{}
	req, _ := ioutil.ReadAll(r.Body)
	err := json.Unmarshal(req, smsRequest)
	if err != nil {
		logrus.WithError(err).Error("can not unmarshal sms template request")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	resp, err := h.budjService.GetSMSTemplate(smsRequest)
	if err != nil {
		logrus.WithError(err).Error("can not get sms temlate from service")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	smsTemplateResponse, err := json.Marshal(resp)
	if err != nil {
		logrus.WithError(err).Error("can not marshal sms template response")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(smsTemplateResponse)
}

func (h *APIHandlers) Shutdown() {
	h.budjService.Shutdown()
}
