package main

import (
	"log"
	"os"
	"time"

	"github.com/pkg/errors"
)

var (
	Version   string
	Commit    string
	BuildTime string
	Hostname  string

	StartTime = time.Now()
)

func init() {
	// If version, commit, or build time are not set, make that clear.
	if Version == "" {
		Version = "unknown"
	}
	if Commit == "" {
		Commit = "unknown"
	}
	if BuildTime == "" {
		BuildTime = "unknown"
	}

	var err error
	Hostname, err = os.Hostname()
	if err != nil {
		log.Print(errors.Wrap(err, "Failed to set Hostname"))
	}
}

func main() {
	Execute(StartTime, Version, Commit, BuildTime, Hostname)
}
