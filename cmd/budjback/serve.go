package main

import (
	"flag"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/spf13/cobra"
	"gitlab.com/budj-budj/budjback/handlers"
)

var (
	serveCmd = &cobra.Command{
		Use:   "serve",
		Short: "start an http server",
		Args:  cobra.ExactArgs(0),
		Run: func(cmd *cobra.Command, args []string) {
			if err := serve(); err != nil {
				log.Fatal(err)
			}
		},
	}

	serveAddr = ":8000"
)

func init() {
	rootCmd.AddCommand(serveCmd)
}

func serve() error {
	apiConfig := handlers.Config{SMSDirectory: "/static/sms"}
	apiHandlers := handlers.NewAPIHandler(apiConfig)
	defer apiHandlers.Shutdown()

	http.Handle("/", getDefaultRouter(apiHandlers))
	log.Printf("HTTP Listening: \t%s", serveAddr)
	return http.ListenAndServe(serveAddr, nil)
}

func getDefaultRouter(handler *handlers.APIHandlers) *mux.Router {

	var staticDirectory string

	flag.StringVar(&staticDirectory, "static", "./static", "the directory to serve files from. Defaults to the current dir")
	flag.Parse()

	router := mux.NewRouter()
	router.HandleFunc("/receipt", handler.AddReceipt).Methods("POST")
	router.HandleFunc("/sms/template", handler.GetSMSTemplate).Methods("POST")
	router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir(
		staticDirectory))))
	return router
}
