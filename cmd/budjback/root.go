package main

import (
	"fmt"
	"log"
	"os"
	"runtime"
	"time"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "budjback",
	Short: "BudjBack",
	Args:  cobra.ExactArgs(0),
}

var buildVars = struct {
	Version   string
	Commit    string
	BuildTime string
	Hostname  string
	StartTime time.Time
}{}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute(StartTime time.Time, Version, Commit, BuildTime, Hostname string) {
	buildVars.Version = Version
	buildVars.Commit = Commit
	buildVars.BuildTime = BuildTime
	buildVars.Hostname = Hostname
	buildVars.StartTime = StartTime

	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func printVersion() {
	log.Printf("Go version: \t%s", runtime.Version())
	log.Printf("Version: \t%s", buildVars.Version)
	log.Printf("Commit: \t%s", buildVars.Commit)
	log.Printf("Built at: \t%s", buildVars.BuildTime)
}
