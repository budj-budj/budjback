ROOT := gitlab.com/budj-budj/budjback
.PHONY: budjback clean lint dependencies update-dependencies

# Project source files
SRCS = $(patsubst ./%,%,$(shell find . -name "*.go" -not -path "*vendor*"))

GO ?= go
GOFMT ?= gofmt
GIT ?= git

FILES ?= ./...
COMMIT := $(shell $(GIT) rev-parse HEAD)
VERSION ?= $(shell $(GIT) describe --tags $(COMMIT) 2> /dev/null || echo "$(COMMIT)")
BUILD_TIME := $(shell LANG=en_US date)
LD_FLAGS := -X 'main.Version=$(VERSION)' -X 'main.Commit=$(COMMIT)' -X 'main.BuildTime=$(BUILD_TIME)'

budjback:
	$(GO_VARS) $(GO) build -i -o="budjback" -ldflags="$(LD_FLAGS)" $(ROOT)/cmd/budjback

fmt: ## to run `go fmt` on all source code
	$(GOFMT) -s -w $(SRCS)

lint:
	@$(GO_VARS) $(GO) get -v honnef.co/go/tools/cmd/unused
	@$(GO_VARS) $(GO) get -v github.com/golang/lint/golint
	@$(GO_VARS) $(GO) get -v github.com/alexkohler/prealloc
	@unused $(shell $(GO_VARS) $(GO) list ./...)
	@golint $(shell go list)
	@prealloc ./...

clean:
	rm -rf ./budjback

dependencies:
	dep ensure

update-dependencies:
	dep ensure --update
