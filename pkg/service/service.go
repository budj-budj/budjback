package service

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/pkg/errors"
)

func NewBudjService(config BudjConfig) (BudjService, error) {
	db, err := gorm.Open("sqlite3", "budj.db")
	if err != nil {
		return nil, errors.Wrap(err, "")
	}
	db.AutoMigrate(&Category{})
	db.AutoMigrate(&User{})
	db.AutoMigrate(&Receipt{})
	db.AutoMigrate(&SubItem{})

	service := &budjService{
		db:     db,
		config: config,
	}
	return service, nil
}

type BudjService interface {
	AddReceipt(r *AddReceiptRequest) (*AddReceiptResponse, error)
	GetSMSTemplate(r *GetSMSTemplateRequest) (*GetSMSTemplateResponse, error)
	Shutdown()
}

var _ BudjService = &budjService{}

type budjService struct {
	db     *gorm.DB
	config BudjConfig
}

func (b *budjService) AddReceipt(r *AddReceiptRequest) (*AddReceiptResponse, error) {
	receipt, err := b.receiptRequestToReceipt(r)
	if err != nil {
		return nil, err
	}

	if err := b.db.Create(receipt).Error; err != nil {
		return nil, errors.Wrapf(err, "cannot insert receipt in database")
	}

	return &AddReceiptResponse{}, nil
}

func (b *budjService) GetSMSTemplate(r *GetSMSTemplateRequest) (*GetSMSTemplateResponse, error) {
	url := b.getSMSTemplateURL(r.AppVersion)

	return &GetSMSTemplateResponse{URL: url}, nil
}

func (b *budjService) receiptRequestToReceipt(req *AddReceiptRequest) (*Receipt, error) {
	user := &User{}
	if err := b.db.First(user, req.UserID).Error; err != nil {
		return nil, errors.Wrapf(err, "cannot find user (userId: %s)", req.UserID)
	}

	subItems, err := b.extractSubItems(req)
	if err != nil {
		return nil, err
	}

	return &Receipt{
		UserID:      user.ID,
		Description: req.Description,
		Amount:      req.Amount,
		Latitude:    req.Location.Latitude,
		Longitude:   req.Location.Longitude,
		SubItems:    subItems,
	}, nil
}

func (b *budjService) extractSubItems(req *AddReceiptRequest) ([]SubItem, error) {

	subItems := []SubItem{}
	for _, subItem := range req.SubItems {
		category, err := b.getCategoryID(subItem.Category)
		if err != nil {
			return nil, err
		}

		subItems = append(subItems, SubItem{
			Title:      subItem.Title,
			Amount:     subItem.Amount,
			CategoryID: category,
		})
	}
	return subItems, nil
}

func (b *budjService) getCategoryID(categoryName string) (uint, error) {
	category := &Category{}
	if err := b.db.First(category, "name = ?", categoryName).Error; err != nil {
		return 0, errors.Wrapf(err, "cannot find category (category: %s)", categoryName)
	}
	return category.ID, nil
}

func (b *budjService) getSMSTemplateURL(appVersion uint) string {
	return fmt.Sprintf("%s/1.js", b.config.SMSDirectory) // TODO: Remove this shit
	// return fmt.Sprintf("%s/%d.js", b.config.SMSDirectory, appVersion)
}

func (b *budjService) Shutdown() {
	b.db.Close()
}
