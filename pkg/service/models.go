package service

import (
	"github.com/jinzhu/gorm"
)

type Receipt struct {
	gorm.Model
	User        User `gorm:"foreignkey:UserID"`
	UserID      uint
	Description string
	Amount      uint
	Latitude    float64
	Longitude   float64
	SubItems    []SubItem
}

type SubItem struct {
	ReceiptID  uint
	Title      string
	Category   Category `gorm:"foreignkey:CategoryID"`
	CategoryID uint
	Amount     *uint
}

type Category struct {
	gorm.Model
	Name string
}

type User struct {
	gorm.Model
	Email     string `gorm:"type:varchar(100);unique_index"`
	Password  string `gorm:"type:text;"`
	FirstName string `gorm:"type:varchar(50);"`
	LastName  string `gorm:"type:varchar(50);"`
}

type Location struct {
	Latitude  float64 `json:"lat"`
	Longitude float64 `json:"long"`
}

type AddReceiptRequest struct {
	UserID    string   `json:"userId"`
	Location  Location `json:"location"`
	Timestamp uint     `json:"time"`
	SubItems  []struct {
		Title    string `json:"title"`
		Category string `json:"category"`
		Amount   *uint  `json:"amount,omitempty"`
	} `json:"subitems"`
	Description string `json:"desc"`
	Amount      uint   `json:"amount"`
}

type AddReceiptResponse struct {
}

type GetSMSTemplateRequest struct {
	AppVersion uint `json:"version"`
}

type GetSMSTemplateResponse struct {
	URL string `json:"url"`
}
